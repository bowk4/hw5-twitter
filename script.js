class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
        this.cardElement = this.createCardElement();
    }

    createCardElement() {
        const cardElement = document.createElement("div");
        cardElement.className = "blog-card";

        const metaDiv = document.createElement("div");
        metaDiv.className = "meta";
        cardElement.appendChild(metaDiv);

        const detailsList = document.createElement("ul");
        detailsList.className = "details";
        metaDiv.appendChild(detailsList);

        const authorListItem = document.createElement("li");
        authorListItem.className = "author";
        authorListItem.textContent = `${this.user.name}`;
        detailsList.appendChild(authorListItem);

        const emailListItem = document.createElement("li");
        emailListItem.className = "email";
        emailListItem.textContent = `${this.user.email}`;
        detailsList.appendChild(emailListItem);

        const hover = document.createElement("li");
        hover.className = "hover";
        hover.textContent = "Hover me";
        detailsList.appendChild(hover);

        const descriptionDiv = document.createElement("div");
        descriptionDiv.className = "description";
        cardElement.appendChild(descriptionDiv);

        const heading = document.createElement("h1");
        heading.textContent = this.post.title;
        descriptionDiv.appendChild(heading);

        const paragraph = document.createElement("p");
        paragraph.textContent = this.post.body;
        descriptionDiv.appendChild(paragraph);

        const closeDiv = document.createElement("div");
        closeDiv.className = "close";
        cardElement.appendChild(closeDiv);

        const closeButton = document.createElement("button");
        const svgElement = document.createElement("ion-icon");
        svgElement.setAttribute("name", "trash-outline");
        closeButton.addEventListener("click", this.askDelete.bind(this));
        closeButton.appendChild(svgElement);
        closeDiv.appendChild(closeButton);

        const editButton = document.createElement("button");
        const svgElementEdit = document.createElement("ion-icon");
        svgElementEdit.setAttribute("name", "create-outline");
        // closeButton.addEventListener("click", this.editCard.bind(this));
        editButton.appendChild(svgElementEdit);
        closeDiv.appendChild(editButton);

        return cardElement;
    }


    askDelete() {
        const confirmationBox = document.querySelector(".confirmation-box");
        const confirmYesButton = document.getElementById("confirm-yes");
        const confirmNoButton = document.getElementById("confirm-no");
        const blurBackground = document.querySelector(".blur-background");

        confirmationBox.style.display = "block";
        blurBackground.style.display = "block";
        body.style.overflow = "hidden"


        confirmYesButton.addEventListener("click", () => {
            const card = document.querySelector(".blog-card");
            if (card) {
                this.deleteCard();
            }
            confirmationBox.style.display = "none";
            blurBackground.style.display = "none";
            body.style.overflow = "auto";
        });

        confirmNoButton.addEventListener("click", () => {
            confirmationBox.style.display = "none";
            blurBackground.style.display = "none";
            body.style.overflow = "auto";
        });
    }

    deleteCard() {
        const postId = this.post.id;
        const cardElement = this.cardElement;

        axios.delete(`https://ajax.test-danit.com/api/json/posts/${postId}`)
            .then(() => {
                const parent = cardElement.parentElement;
                if (parent) {
                    parent.removeChild(cardElement);
                }
            })
            .catch((error) => {
                console.error("Помилка при видаленні картки:", error);
            });
    }
}


function getUsers() {
    return axios.get("https://ajax.test-danit.com/api/json/users");
}

function getPosts() {
    return axios.get("https://ajax.test-danit.com/api/json/posts");
}

const postsContainer = document.getElementById("posts-container");
const body = document.querySelector("body")
const loader = document.createElement("div");
loader.className = "lds-dual-ring";
// loader.innerHTML = ['<div></div>', '<div></div>'].join('');
body.appendChild(loader);
body.style.overflow = "hidden"

function displayPosts(posts, users) {
    body.removeChild(loader);
    body.style.overflow = "auto"
    posts.forEach((post) => {
        const user = users.find((u) => u.id === post.id);
        const card = new Card(post, user);
        const cardElement = card.cardElement;
        postsContainer.appendChild(cardElement);
    });
}

Promise.all([getUsers(), getPosts()])
    .then((results) => {
        const users = results[0].data;
        const posts = results[1].data;
        displayPosts(posts, users);
    });








const addPostButton = document.querySelector(".addPost");
const addPostModal = document.getElementById("addPostModal");
const closeModalButton = document.getElementById("closeModal");
const postForm = document.getElementById("postForm");
const blurBackground = document.querySelector(".blur-background");

addPostButton.addEventListener("click", () => {
    addPostModal.style.display = "block";
    blurBackground.style.display = "block";
    body.style.overflow = "hidden"
});

closeModalButton.addEventListener("click", () => {
    addPostModal.style.display = "none";
    blurBackground.style.display = "none";
    body.style.overflow = "auto"
});

postForm.addEventListener("submit", (event) => {
    event.preventDefault();
    const title = document.getElementById("title").value;
    const bodytxt = document.getElementById("body").value;
    const nameUser = document.getElementById("name").value;
    const emailUser = document.getElementById("email").value;

    // Отримати потрібного користувача (з id: 1) для авторства
    const authorId = 1;

    axios.post("https://ajax.test-danit.com/api/json/posts", {
        title,
        bodytxt,
        nameUser,
        emailUser,
        userId: authorId,
    })
        .then((response) => {
            // Успішно створено на сервері
            addPostModal.style.display = "none";
            blurBackground.style.display = "none";
            body.style.overflow = "auto"

            // Оновити відображення публікацій на сторінці
            const newPost = response.data;
            const user = getUserById(authorId); // Визначте цю функцію відповідно до вашої реалізації
            displayNewPost(newPost, user); // Визначте цю функцію для відображення нової публікації
        })
        .catch((error) => {
            console.error("Помилка при створенні публікації:", error);
        });
});

async function getUserById(userId) {
    try {
        const response = await axios.get(`https://ajax.test-danit.com/api/json/users/${userId}`);
        return response.data; // Повертаємо знайденого користувача
    } catch (error) {
        console.error(`Помилка при отриманні користувача з id ${userId}: ${error}`);
        return null; // Повертаємо null у разі помилки або якщо користувача не знайдено
    }
}


function displayNewPost(post, user) {

    const postsContainer = document.getElementById("posts-container");

    const newCard = document.createElement("div");
    newCard.className = "blog-card";

    const metaDiv = document.createElement("div");
    metaDiv.className = "meta";
    newCard.appendChild(metaDiv);

    const detailsList = document.createElement("ul");
    detailsList.className = "details";
    metaDiv.appendChild(detailsList);

    const authorListItem = document.createElement("li");
    authorListItem.className = "author";
    authorListItem.textContent = post.nameUser;
    detailsList.appendChild(authorListItem);

    const emailListItem = document.createElement("li");
    emailListItem.className = "email";
    emailListItem.textContent = post.emailUser;
    detailsList.appendChild(emailListItem);

    const hover = document.createElement("li");
    hover.className = "hover";
    hover.textContent = "Hover me";
    detailsList.appendChild(hover);

    const descriptionDiv = document.createElement("div");
    descriptionDiv.className = "description";
    newCard.appendChild(descriptionDiv);

    const heading = document.createElement("h1");
    heading.textContent = post.title;
    descriptionDiv.appendChild(heading);

    const paragraph = document.createElement("p");
    paragraph.textContent = post.bodytxt;
    descriptionDiv.appendChild(paragraph);

    const closeDiv = document.createElement("div");
    closeDiv.className = "close";
    newCard.appendChild(closeDiv);

    const closeButton = document.createElement("button");
    const svgElement = document.createElement("ion-icon");
    svgElement.setAttribute("name", "trash-outline");
    closeButton.addEventListener("click", () => deleteCards());
    closeButton.appendChild(svgElement);
    closeDiv.appendChild(closeButton);

    const editButton = document.createElement("button");
    const svgElementEdit = document.createElement("ion-icon");
    svgElementEdit.setAttribute("name", "create-outline");
    // closeButton.addEventListener("click", this.editCard.bind(this));
    editButton.appendChild(svgElementEdit);
    closeDiv.appendChild(editButton);

    postsContainer.insertBefore(newCard, postsContainer.firstChild);
}

// function deleteCards() {
//     const postId = this.post.id
//     const cardElement = this.cardElement;
//
//     axios.delete(`https://ajax.test-danit.com/api/json/posts/${postId}`)
//         .then(() => {
//             const parent = cardElement.parentElement;
//             if (parent) {
//                 parent.removeChild(cardElement);
//             }
//         })
//         .catch((error) => {
//             console.error("Помилка при видаленні картки:", error);
//         });
// }







